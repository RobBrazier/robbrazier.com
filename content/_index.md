---
framed: true
---
# Hello!
Welcome to my personal website! There aren't many posts at the moment, however you can see [who I am](/about) and [what projects I've been working on](/projects) from the links above