---
title: EntityDecode
description: A Real-time HTML entity encoder/decoder
categories:
- Website
- Open Source
featuredImage: "images/entitydecode-com.png"
aliases: 
- "/portfolio/entitydecode"
---
A Real-time HTML entity encoder/decoder (everything is done on the client-side, no server-side calls required).

The application is built with [Svelte](https://svelte.dev) and the source is available on [Codeberg](https://codeberg.org/RobBrazier/entitydecode.com)

[Go to Site &raquo;](https://entitydecode.com)
