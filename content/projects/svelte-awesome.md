---
title: Svelte-Awesome
description: A Font-Awesome component for Svelte, using inline SVG
categories:
- Open Source

---
A Font-Awesome component for [Svelte](https://svelte.dev), using inline SVG

This project was based on [Justineo/vue-awesome](https://github.com/Justineo/vue-awesome) and made to work natively with Svelte and recently Sapper.

Supports both FontAwesome v4 icons (built in) and FontAwesome v5 (by importing the official `@fortawesome/*` packages

[Go to Repo &raquo;](https://github.com/RobBrazier/svelte-awesome)